// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Phoenix : ModuleRules
{
	public Phoenix(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "AIModule", "InputCore", "GameplayTasks","NavigationSystem", "HeadMountedDisplay" });

		PublicIncludePaths.AddRange(new string[] { "Phoenix/" });
	}
}

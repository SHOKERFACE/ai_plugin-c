// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EAIWeponTypes : uint8
{
	AssaultRifle UMETA(DisplayName = "Assault Rifle"),
	MachineGun UMETA(DisplayName = "Machine Gun"),
	SniperRifle UMETA(DisplayName = "Sniper Rifle"),
	
};


UENUM(BlueprintType)
enum class EAIStates : uint8
{
	Idle = 0 UMETA(DisplayName = "Idle"),
	Patrol = 1 UMETA(DisplayName = "Patrol"),
	Attack = 2 UMETA(DisplayName = "Attack"),
	Investigation = 3 UMETA(DisplayName = "Investigation"),
	InvestigateDeadBody = 4 UMETA(DisplayName = "InvestigateDeadBody")
};


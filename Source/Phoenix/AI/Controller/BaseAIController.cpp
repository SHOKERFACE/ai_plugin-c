// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Controller/BaseAIController.h"

#include "DrawDebugHelpers.h"
#include "AI/T_DeadBodyAlert.h"
#include "AI/Characters/AIBaseCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AIPerceptionSystem.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Sight.h"
#include "Containers/Array.h"

ABaseAIController::ABaseAIController()
{
	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));
	
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));
	BlackboardComponent->SetAutoActivate(true);
	PerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &ABaseAIController::OnPerceptionUpdate);

	
}

void ABaseAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	AIChar = Cast<AAIBaseCharacter>(InPawn);
	if (AIChar)
	{
		Debug = AIChar->ShowDebug;
		this->RunBehaviorTree(AIChar->BehaviorTree);
	}
	PerceptionComponent->SetSenseEnabled(UAISense_Hearing::StaticClass(), AIChar->UseHearing);
	ChangeAIState(EAIStates::Attack);

}

void ABaseAIController::OnPerceptionUpdate(const TArray<AActor*>& UpdatedActors)
{
	FActorPerceptionBlueprintInfo ActorPerceptionInfoArray;
	for (AActor* UpdatedActor : UpdatedActors )
	{
		PerceptionComponent->GetActorsPerception(UpdatedActor,ActorPerceptionInfoArray);
		auto LastStimuliArr = ActorPerceptionInfoArray.LastSensedStimuli ;
		for (  auto Stimuli : LastStimuliArr)
		{

			TSubclassOf<UAISense> SenseClass = UAIPerceptionSystem::GetSenseClassForStimulus(GetWorld(),Stimuli);
			
			if(SenseClass == UAISense_Sight::StaticClass())
			{
				if(ActorPerceptionInfoArray.Target == UGameplayStatics::GetPlayerCharacter(GetWorld(),0))
				{
					if(Stimuli.WasSuccessfullySensed())
					{
						PlayerInLineOfSight(true);
						ChangeAIState(EAIStates::Attack);
					}
					else
					{
						PlayerInLineOfSight(false);
					}
								
				}
				
				if(!isAlreadyInCombat() && AIChar->DetectDeadBody)
				{
					AT_DeadBodyAlert* DeadBody = Cast<AT_DeadBodyAlert>(ActorPerceptionInfoArray.Target);
					if(DeadBody)
					{
						
						if (!DeadBodyArray.Contains(DeadBody))
						{
							DeadBodyArray.AddUnique(DeadBody);
							if (Stimuli.WasSuccessfullySensed())
							{
								UpdateLastSensPosition(Stimuli.StimulusLocation, Debug, FColor::Red, FString("DeadBody"));
								ChangeAIState(EAIStates::Investigation);
							}
						}
						
					}
				}
			}
			else if(SenseClass == UAISense_Damage::StaticClass())
			{
				if(!isAlreadyInCombat() && 	Stimuli.WasSuccessfullySensed())
				{
					UpdateLastSensPosition(Stimuli.StimulusLocation, Debug, FColor::Red, FString("Damage Causer"));
					ChangeAIState(EAIStates::Attack);

				}
			}
			else if(SenseClass == UAISense_Hearing::StaticClass())
			{
				if(!isAlreadyInCombat() && 	Stimuli.WasSuccessfullySensed())
				{
					if(!isAlreadyInCombat() && 	Stimuli.WasSuccessfullySensed())
					{
						UpdateLastSensPosition(Stimuli.StimulusLocation, Debug, FColor::Blue, FString("Noise Location"));
						ChangeAIState(EAIStates::Attack);

					}
				}
			}
		}
	}
}
bool ABaseAIController::isAlreadyInCombat()
{
	//If AI on Attack state dont react to Dead TeamMate
	return BlackboardComponent->GetValueAsEnum(FName("EnumAIState")) == 2;
}

void ABaseAIController::PlayerInLineOfSight(bool CanSeePlayer)
{
	BlackboardComponent->SetValueAsBool(FName("CanSeePlayer"), CanSeePlayer);
}

void ABaseAIController::ChangeAIState(EAIStates State)
{
	BlackboardComponent->SetValueAsEnum(FName("EnumAIState"),uint8(State));
}

void ABaseAIController::UpdateLastSensPosition(FVector& Stimuluslocation, bool ShowDebug,
	FColor DebugSphereColor, FString DebugStringText)
{
	BlackboardComponent->SetValueAsVector(FName("LastSeenPosition"),Stimuluslocation);
	if(ShowDebug)
	{
		DrawDebugSphere(GetWorld(),Stimuluslocation, 20.0f, 12, DebugSphereColor, false, 2.0f, 1.0f);
		DrawDebugString(GetWorld(),Stimuluslocation + FVector(0.0f,0.0f,25.0f),DebugStringText,
						nullptr,DebugSphereColor, 2.0f);
	}
}



// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Structs/Types.h"
#include "BaseAIController.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API ABaseAIController : public AAIController
{
	
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
	UAIPerceptionComponent* MyPerceptionComponent = PerceptionComponent;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool Debug;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TArray<class AT_DeadBodyAlert*> DeadBodyArray;

	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	UBlackboardComponent* BlackboardComponent;

	UFUNCTION(BlueprintCallable)
	void ChangeAIState(EAIStates State);
protected:

	ABaseAIController();

	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION(BlueprintCallable)
	void OnPerceptionUpdate(const TArray<AActor*>& UpdatedActors);

	UFUNCTION(BlueprintCallable)
	bool isAlreadyInCombat();

	UFUNCTION(BlueprintCallable)
	void PlayerInLineOfSight(bool CanSeePlayer);

	

	UFUNCTION(BlueprintCallable)
	void UpdateLastSensPosition(FVector& Stimuluslocation, bool ShowDebug, FColor DebugSphereColor, FString DebugStringText);
	
	UPROPERTY()
	class AAIBaseCharacter* AIChar;
};

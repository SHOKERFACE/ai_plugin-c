// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "T_DeadBodyAlert.generated.h"

UCLASS()
class PHOENIX_API AT_DeadBodyAlert : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AT_DeadBodyAlert();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	class USphereComponent* Sphere;

	
	UPROPERTY(EditAnywhere)
	class UAIPerceptionStimuliSourceComponent* StimuliSourceComponent;
};

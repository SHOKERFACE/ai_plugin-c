// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseBulletProjectaile.generated.h"

UCLASS()
class PHOENIX_API ABaseBulletProjectaile : public AActor
{
	GENERATED_BODY()
	
public:	
	ABaseBulletProjectaile();

protected:

	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
	class USphereComponent* CollisionComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
	class UProjectileMovementComponent* MovementComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
	UStaticMeshComponent* ProjectileSphere;
	
	UFUNCTION()
	void OnProjectileHit(UPrimitiveComponent* HitComponent,
									 AActor* OtherActor,
									 UPrimitiveComponent* OtherComp,
									 FVector NormalImpulse,
									 const FHitResult& Hit);

};


#include "AI/Characters/AIBaseCharacter.h"
#include "AIController.h"
#include "BrainComponent.h"
#include "Components/CapsuleComponent.h"
#include "AI/Controller/BaseAIController.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Sight.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"

AAIBaseCharacter::AAIBaseCharacter()
{
	PrimaryActorTick.bCanEverTick = false;
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(GetMesh(),TEXT("weapon_rSocket"));
	AIControllerClass = ABaseAIController::StaticClass();
	bUseControllerRotationYaw = false;
	this->OnTakeAnyDamage.AddDynamic(this, &AAIBaseCharacter::HandleAnyDamage);
}

void AAIBaseCharacter::WeaponSetup()
{
	WeaponMesh->SetWorldScale3D(FVector(1.0f));
	WeaponMesh->SetRelativeLocationAndRotation(FVector(-2.0f,-28.0f,-4.0f),FRotator(0.0f));
	WeaponFireRate = 0.5f;
	WeaponDamage = 3.0f;
	MaxAmmo=10;
	AmmoInMag=MaxAmmo;
	WeaponRecoil=10.0f;
	WeaponAimTime=0.3f;
	ReloadAnimPlayRate=1.0f;
}


void AAIBaseCharacter::OnDead()
{
	GetMesh()->SetCollisionResponseToChannel(ECC_Pawn,ECR_Overlap);

	AAIController* MyAIController = Cast<AAIController>(GetController());
	if (MyAIController != nullptr)
	{
		UBrainComponent* BrainComp = MyAIController->GetBrainComponent();
		if (BrainComp != nullptr)
		{
			BrainComp->StopLogic(TEXT("Some reason"));
		}
	}

	GetMesh()->SetSimulatePhysics(true);
	WeaponMesh->SetSimulatePhysics(true);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	MyAIController->GetAIPerceptionComponent()->SetSenseEnabled(UAISense_Sight::StaticClass(), false);
	MyAIController->GetAIPerceptionComponent()->SetSenseEnabled(UAISense_Hearing::StaticClass(), false);
	MyAIController->GetAIPerceptionComponent()->SetSenseEnabled(UAISense_Damage::StaticClass(), false);

	if (DeadBody)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride=ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParameters.Owner = GetOwner();
		SpawnParameters.Instigator = GetInstigator();
		const FTransform SpawnTransform = FTransform((FVector(0.0f), FRotator(0.0f), FVector(1.0f)));;
	
		AActor* DeadBodyRef = GetWorld()->SpawnActor(DeadBody,&SpawnTransform,SpawnParameters);
		DeadBodyRef->AttachToComponent(GetMesh(),FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true));
	}
	
	
}

void AAIBaseCharacter::WeaponReload()
{
	if (!isReloading || IsDead)
	{
		isReloading = true;
		if(GetMesh()->GetAnimInstance() && WeaponReloadMontage)
		{
			UAnimInstance* myAnimInst = GetMesh()->GetAnimInstance();
			myAnimInst->Montage_Play(WeaponReloadMontage,ReloadAnimPlayRate); 
			myAnimInst->OnMontageEnded.AddDynamic(this, &AAIBaseCharacter::Event_OnMontageEnded);
			
		}
	}
}

void AAIBaseCharacter::Event_OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	GetMesh()->GetAnimInstance()->OnMontageEnded.RemoveDynamic(this, &AAIBaseCharacter::Event_OnMontageEnded);

	if (WeaponReloadMontage == Montage)
	{
		AmmoInMag = MaxAmmo;
		isReloading = false;
	}
}

void AAIBaseCharacter::SetDefaultAIState()
{
	if (MyBlackboard){return;}
	MyBlackboard->SetValueAsEnum(FName("EnumAIState"),1);
}

void AAIBaseCharacter::HandleAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if(IsDead){return;}
	EnemyHP = (Damage >= EnemyHP) ? 0.0f : EnemyHP - Damage;
	if (EnemyHP <= 0.0f)
	{
		IsDead = true;
		OnDead();
	}
}

void AAIBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnDefaultController();
	MyBlackboard = Cast<ABaseAIController>(GetController())->GetBlackboardComponent();
	if(MyBlackboard)
	{
		WeaponSetup();
		MyBlackboard->SetValueAsVector(FName("SpawnLocation"), GetActorLocation());
		PlayerRef = UGameplayStatics::GetPlayerCharacter(GetWorld(),0);	
		if (PathPatrol)
		{
			MyBlackboard->SetValueAsObject(FName("EnemyActor"),PlayerRef);
		}
		MyBlackboard->SetValueAsBool(FName("PathPatrol"),true);
	}	
	SetDefaultAIState();

}

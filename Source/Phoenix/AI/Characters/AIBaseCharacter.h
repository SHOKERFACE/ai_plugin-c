// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/T_DeadBodyAlert.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/Character.h"
#include "Structs/Types.h"
#include "Components/SkeletalMeshComponent.h"
#include "AIBaseCharacter.generated.h"

UCLASS()
class PHOENIX_API AAIBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIBaseCharacter();

	UFUNCTION(BlueprintCallable)
	void WeaponSetup();

	UFUNCTION(BlueprintCallable)
	void SetDefaultAIState();

	UFUNCTION()
	void HandleAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy,
	                     AActor* DamageCauser);

	UFUNCTION(BlueprintCallable)
	void OnDead();

	UFUNCTION(BlueprintCallable)
	void WeaponReload();
	
	UFUNCTION(BlueprintCallable)
	void Event_OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);

	UPROPERTY(VisibleAnywhere,  Category = "Components")
	USkeletalMeshComponent* WeaponMesh = nullptr;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	
public:

	UPROPERTY(EditDefaultsOnly, Category = "DeadBody")
	TSubclassOf<AT_DeadBodyAlert> DeadBody = nullptr;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere,Category="AI BT And Weapon Setup")
	float EnemyHP = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="AI BT And Weapon Setup")
	UBehaviorTree* BehaviorTree;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere,meta=(ExposeOnSpawn) ,Category="AI BT And Weapon Setup")
	EAIWeponTypes WeaponType = EAIWeponTypes::AssaultRifle;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Patrol and Assault Path setup")
	float RandomPatrolRadius = 200.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Patrol and Assault Path setup")
	bool PathPatrol = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Patrol and Assault Path setup")
	TArray<ATargetPoint*> TargetPoints;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Patrol and Assault Path setup")
	ATargetPoint* AssaultPath;

	UPROPERTY(BlueprintReadWrite,Category="Weapon Setup Variables")
	float WeaponDamage = 10.0f;

	UPROPERTY(BlueprintReadWrite,Category="Weapon Setup Variables")
	int TargetPointIndex = 0;
	
	UPROPERTY(BlueprintReadWrite,Category="Weapon Setup Variables")
	float WeaponFireRate = 10.0f;

	UPROPERTY(BlueprintReadWrite,Category="Weapon Setup Variables")
	int MaxAmmo = 5;

	UPROPERTY(BlueprintReadWrite,Category="Weapon Setup Variables")
	int AmmoInMag = MaxAmmo;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="Weapon Setup Variables")
	USoundBase* WeaponSound;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Setup Variables")
	float WeaponRecoil = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="Weapon Setup Variables")
	float WeaponAimTime = 10.0f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Setup Variables")
	float ReloadAnimPlayRate = 0.9f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category="Weapon Setup Variables")
	UAnimMontage* WeaponReloadMontage;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AI Perception setup")
	float  VisionAngle = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere,Category="AI Perception setup")
	bool UseHearing = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="AI Perception setup")
	bool DetectDeadBody = false;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	bool IsDead = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Ironsight = false;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TSubclassOf<AActor> Bullet;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	ACharacter* PlayerRef = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category="AI Perception setup")
	bool isReloading = false;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category="AI Perception setup")
	bool IseePlayer = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="AI Perception setup")
	bool ShowDebug = false;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly,Category="AI Perception setup")
	UBlackboardComponent* MyBlackboard = nullptr;
};

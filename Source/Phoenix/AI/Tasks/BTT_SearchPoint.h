// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_SearchPoint.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API UBTT_SearchPoint : public UBTTaskNode
{
	GENERATED_BODY()

public:

	UBTT_SearchPoint();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};

#include "AI/Tasks/BTT_RotateToEnemy.h"
#include "AIController.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

UBTT_RotateToEnemy::UBTT_RotateToEnemy()
{
	NodeName = "Rotate To Enemy";
}

EBTNodeResult::Type UBTT_RotateToEnemy::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{

	const auto Controller = OwnerComp.GetAIOwner();
	if(!Controller) return EBTNodeResult::Failed;

	AActor* Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if(!Player) return EBTNodeResult::Failed;

	Controller->SetFocus(Player, EAIFocusPriority::Move);

	return EBTNodeResult::Succeeded;
}

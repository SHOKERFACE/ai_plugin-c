// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/BTT_FindPathPatrolLocation.h"

#include "AIController.h"
#include "AI/Characters/AIBaseCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTT_FindPathPatrolLocation::UBTT_FindPathPatrolLocation()
{
	NodeName = "Find Path Patrol Location";
}

EBTNodeResult::Type UBTT_FindPathPatrolLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if(!Blackboard) return  EBTNodeResult::Failed;
	
	const auto Controller = OwnerComp.GetAIOwner();
	if(!Controller) return EBTNodeResult::Failed;

	const auto Pawn = Controller->GetPawn();
	if(!Pawn) return  EBTNodeResult::Failed;

	AAIBaseCharacter* AIChar = Cast<AAIBaseCharacter>(Pawn);
	if(!AIChar) return  EBTNodeResult::Failed;

	float LastPointIndex = AIChar->TargetPointIndex;

	if (AIChar->TargetPoints.Num()>0)
	{
		if(!AIChar->TargetPoints.IsValidIndex(LastPointIndex)){	LastPointIndex = 0;}

		Blackboard->SetValueAsVector(NewPatrolLocation.SelectedKeyName,
									AIChar->TargetPoints[LastPointIndex]->GetActorLocation());

		AIChar->TargetPointIndex = LastPointIndex + 1;
		return  EBTNodeResult::Succeeded;
	}
	
	return  EBTNodeResult::Succeeded;
	
	
}

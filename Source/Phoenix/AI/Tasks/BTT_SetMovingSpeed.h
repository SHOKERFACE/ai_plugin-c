// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_SetMovingSpeed.generated.h"

UCLASS()
class PHOENIX_API UBTT_SetMovingSpeed : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTT_SetMovingSpeed();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float WalkSpeed = 400.0f;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_RotateToEnemy.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API UBTT_RotateToEnemy : public UBTTaskNode
{
	GENERATED_BODY()

public:

	UBTT_RotateToEnemy();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

};

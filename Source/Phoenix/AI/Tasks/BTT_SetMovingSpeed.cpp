// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/BTT_SetMovingSpeed.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AIController.h"
#include "AI/Characters/AIBaseCharacter.h"

UBTT_SetMovingSpeed::UBTT_SetMovingSpeed()
{
	NodeName = "Set Moving Speed";
}

EBTNodeResult::Type UBTT_SetMovingSpeed::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Controller = OwnerComp.GetAIOwner();
	if(!Controller) return EBTNodeResult::Failed;
	
	const auto Pawn = Controller->GetPawn();
	if(!Pawn) return  EBTNodeResult::Failed;

	AAIBaseCharacter* Char = Cast<AAIBaseCharacter>(Pawn);
	Char->GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	return  EBTNodeResult::Succeeded;

}

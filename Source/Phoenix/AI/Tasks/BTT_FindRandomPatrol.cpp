// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/BTT_FindRandomPatrol.h"

#include "AIController.h"
#include "NavigationSystem.h"
#include "AI/Characters/AIBaseCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTT_FindRandomPatrol::UBTT_FindRandomPatrol()
{
	NodeName = "Find Random Patrol";
}

EBTNodeResult::Type UBTT_FindRandomPatrol::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if(!Blackboard) return  EBTNodeResult::Failed;
	
	const auto Controller = OwnerComp.GetAIOwner();
	if(!Controller) return EBTNodeResult::Failed;

	const auto Pawn = Controller->GetPawn();
	if(!Pawn) return  EBTNodeResult::Failed;

	AAIBaseCharacter* AIChar = Cast<AAIBaseCharacter>(Pawn);
	if(!AIChar) return  EBTNodeResult::Failed;

	FVector SpawnLocation = Blackboard->GetValueAsVector(FName("SpawnLocation"));

	FNavLocation NavLocation;
	
	const bool Found = UNavigationSystemV1::GetCurrent(Pawn)->GetRandomPointInNavigableRadius(SpawnLocation,
											AIChar->RandomPatrolRadius, NavLocation);

	if (Found)
	{
		Blackboard->SetValueAsVector(PatrolLocation.SelectedKeyName,NavLocation);
		return  EBTNodeResult::Succeeded;
	}

	return  EBTNodeResult::Failed;
}

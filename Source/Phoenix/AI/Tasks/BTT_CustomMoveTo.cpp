#include "AI/Tasks/BTT_CustomMoveTo.h"
#include "AIController.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/Characters/AIBaseCharacter.h"
#include "EnvironmentQuery/EnvQueryTypes.h"

UBTT_CustomMoveTo::UBTT_CustomMoveTo()
{
	NodeName = "Custom Move To";
}



EBTNodeResult::Type UBTT_CustomMoveTo::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	MyController = OwnerComp.GetAIOwner();
	if(!MyController) return EBTNodeResult::Failed;

	const auto Pawn = MyController->GetPawn();
	if(!Pawn) return  EBTNodeResult::Failed;

	const auto Blackboard = OwnerComp.GetBlackboardComponent();

	if(!Blackboard) return  EBTNodeResult::Failed;


	FVector Origin = Blackboard->GetValueAsVector(MoveToLocation.SelectedKeyName);
	
	FNavLocation NavLocation;
	
	const auto Found = UNavigationSystemV1::GetCurrent(Pawn)->GetRandomPointInNavigableRadius(Origin,
											RandomPointInNavigableRadius, NavLocation);

	if (!Found)
	{
		UE_LOG(LogTemp, Verbose, TEXT("Not found Location"))
		return  EBTNodeResult::Failed;
	}
	OwnerComp.GetAIOwner()->MoveToLocation(NavLocation,50.0f, false);
	OwnerComp.GetAIOwner()->ReceiveMoveCompleted.AddDynamic(this, &UBTT_CustomMoveTo::OnMoveCompleted);

	return EBTNodeResult::InProgress;
}


void UBTT_CustomMoveTo::OnMoveCompleted(const FAIRequestID RequestID, const EPathFollowingResult::Type Result)
{
	UBehaviorTreeComponent* OwnerComp = Cast<UBehaviorTreeComponent>(MyController->GetBrainComponent());
	if (!OwnerComp) {return;}
	if(!MyController){FinishLatentTask(*OwnerComp, EBTNodeResult::Failed);}
	MyController->ReceiveMoveCompleted.RemoveDynamic(this, &UBTT_CustomMoveTo::OnMoveCompleted);
	
	if (Result == EPathFollowingResult::Success)
	{
		FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);
	}
	else
	{
		FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);

	}
}

	



// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "Structs/Types.h"
#include "BTT_ChangeAiState.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API UBTT_ChangeAiState : public UBTTaskNode
{
	GENERATED_BODY()

public:

	UBTT_ChangeAiState();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	EAIStates AiState;

	UPROPERTY()
    class ABaseAIController* Controller = nullptr;

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/BTT_SearchPoint.h"

#include "AIController.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTT_SearchPoint::UBTT_SearchPoint()
{
	NodeName = "Search Point";
}

EBTNodeResult::Type UBTT_SearchPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if(!Blackboard) return  EBTNodeResult::Failed;

	const auto Controller = OwnerComp.GetAIOwner();
	if(!Controller) return EBTNodeResult::Failed;

	const auto Pawn = Controller->GetPawn();
	if(!Pawn) return  EBTNodeResult::Failed;

	FNavLocation NavLocation;
	FVector Origin = Blackboard->GetValueAsVector(FName("LastSeenPosition")); 
	
	const auto Found = UNavigationSystemV1::GetCurrent(Pawn)->GetRandomPointInNavigableRadius(Origin,
											200.0f, NavLocation);

	if (!Found){return EBTNodeResult::Failed;}
	Blackboard->SetValueAsVector(FName("SearchPoint"),NavLocation);
	return EBTNodeResult::Succeeded;
}

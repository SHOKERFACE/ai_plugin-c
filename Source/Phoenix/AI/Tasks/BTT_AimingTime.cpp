// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/BTT_AimingTime.h"

#include "AIController.h"
#include "AI/Characters/AIBaseCharacter.h"
#include "Kismet/KismetSystemLibrary.h"

UBTT_AimingTime::UBTT_AimingTime()
{
	NodeName = "AimingTime";
}

EBTNodeResult::Type UBTT_AimingTime::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	MyController = OwnerComp.GetAIOwner();
	if(!MyController) return EBTNodeResult::Failed;

	const auto Pawn = MyController->GetPawn();
	if(!Pawn) return  EBTNodeResult::Failed;

	AAIBaseCharacter* AIChar = Cast<AAIBaseCharacter>(Pawn);
	if(!AIChar) return  EBTNodeResult::Failed;

	if(AIChar->Ironsight)
	{
		return EBTNodeResult::Succeeded;
	}

	AIChar->Ironsight = true;
	
	
	
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(TimerHandle, this, &UBTT_AimingTime::OnTimerExpired, AIChar->WeaponAimTime, false);
	
	return EBTNodeResult::InProgress;
}

void UBTT_AimingTime::OnTimerExpired()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	UBehaviorTreeComponent* OwnerComp = Cast<UBehaviorTreeComponent>(MyController->GetBrainComponent());

	if (OwnerComp)
	{
		UE_LOG(LogTemp, Verbose, TEXT("found OwnerComp"))
		FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);
	}
	else
	{
		UE_LOG(LogTemp, Verbose, TEXT("Not found OwnerComp"))
	}
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_AimingTime.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API UBTT_AimingTime : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTT_AimingTime();
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	void OnTimerExpired();

	UPROPERTY()
	FTimerHandle TimerHandle;

	UPROPERTY()
	AAIController* MyController;
};

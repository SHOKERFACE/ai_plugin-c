// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_FindPathPatrolLocation.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API UBTT_FindPathPatrolLocation : public UBTTaskNode
{
	GENERATED_BODY()

public:


	UBTT_FindPathPatrolLocation();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FBlackboardKeySelector NewPatrolLocation;
};

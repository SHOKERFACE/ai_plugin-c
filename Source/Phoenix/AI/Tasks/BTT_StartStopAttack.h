// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_StartStopAttack.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API UBTT_StartStopAttack : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTT_StartStopAttack();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UPROPERTY()
	class AAIController* Controller;

	UPROPERTY()
	class AAIBaseCharacter* AIChar;

	
	
	void OnTimerExpired();

	UPROPERTY()
	FTimerHandle TimerHandle;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	bool StartAttack = true;
	void StartShoot();

	void StopShoot();
};

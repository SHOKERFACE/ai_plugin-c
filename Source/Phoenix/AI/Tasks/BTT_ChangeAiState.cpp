

#include "AI/Tasks/BTT_ChangeAiState.h"

#include "AI/Controller/BaseAIController.h"


UBTT_ChangeAiState::UBTT_ChangeAiState()
{
	NodeName = "Change AI State";
}

EBTNodeResult::Type UBTT_ChangeAiState::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{

	Controller = Cast<ABaseAIController>(OwnerComp.GetAIOwner());
	if(!Controller) return EBTNodeResult::Failed;

	Controller->ChangeAIState(AiState);
	return  EBTNodeResult::Succeeded;
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/BTT_StartStopAttack.h"

#include "AIController.h"
#include "AI/BaseBulletProjectaile.h"
#include "AI/Characters/AIBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

UBTT_StartStopAttack::UBTT_StartStopAttack()
{
	NodeName = "Start Stop Attack";
}

EBTNodeResult::Type UBTT_StartStopAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if(!Blackboard) return  EBTNodeResult::Failed;
	
	Controller = OwnerComp.GetAIOwner();
	if(!Controller) return EBTNodeResult::Failed;

	const auto Pawn = Controller->GetPawn();
	if(!Pawn) return  EBTNodeResult::Failed;

	AIChar = Cast<AAIBaseCharacter>(Pawn);
	if(!AIChar || AIChar->isReloading) return  EBTNodeResult::Failed;

	if (StartAttack)
	{
		StartShoot();
		return EBTNodeResult::InProgress;
	}
	else
	{
		StopShoot();
	}
	return EBTNodeResult::InProgress;
}

void UBTT_StartStopAttack::StartShoot()
{
	Controller->ClearFocus(EAIFocusPriority::Move);
	AIChar->Ironsight = false;
	UBehaviorTreeComponent* OwnerComp = Cast<UBehaviorTreeComponent>(Controller->GetBrainComponent());
	if (!OwnerComp){return;}
	
	if(AIChar->isReloading || AIChar->IsDead){FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);}

	AIChar->Ironsight = true;
	FVector SocketLocation = AIChar->WeaponMesh->GetSocketLocation("Muzzle");
	FVector Recoil = FVector(UKismetMathLibrary::RandomFloatInRange(AIChar->WeaponRecoil * -1,AIChar->WeaponRecoil),
							 UKismetMathLibrary::RandomFloatInRange(AIChar->WeaponRecoil * -1,AIChar->WeaponRecoil),
							 UKismetMathLibrary::RandomFloatInRange(AIChar->WeaponRecoil * -1,AIChar->WeaponRecoil));
	FVector TraceEndLoc = UGameplayStatics::GetPlayerCharacter(GetWorld(),0)->GetActorLocation()  + Recoil;	
	FHitResult HitResult;
	bool Success = GetWorld()->LineTraceSingleByChannel(HitResult, SocketLocation, TraceEndLoc,ECC_Visibility);
	UGameplayStatics::PlaySound2D(GetWorld(),AIChar->WeaponSound,0.5f);
	UGameplayStatics::ApplyDamage(HitResult.Actor.Get(), AIChar->WeaponDamage, nullptr, AIChar,nullptr);
	AIChar->AmmoInMag--;

	FRotator ShootRotation = UKismetMathLibrary::FindLookAtRotation(SocketLocation, HitResult.TraceEnd);
	GetWorld()->SpawnActor<ABaseBulletProjectaile>(AIChar->Bullet,SocketLocation ,ShootRotation);
	
	if (AIChar->AmmoInMag <= 0)
	{
		AIChar->WeaponReload();
		FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);
	}
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(TimerHandle, this, &UBTT_StartStopAttack::OnTimerExpired, AIChar->WeaponFireRate, false);
	
}

void UBTT_StartStopAttack::StopShoot()
{
	Controller->ClearFocus(EAIFocusPriority::Move);
	AIChar->Ironsight = false;
	UBehaviorTreeComponent* OwnerComp = Cast<UBehaviorTreeComponent>(Controller->GetBrainComponent());

	if (OwnerComp){FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);}
	
}

void UBTT_StartStopAttack::OnTimerExpired()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	UBehaviorTreeComponent* OwnerComp = Cast<UBehaviorTreeComponent>(Controller->GetBrainComponent());

	if (OwnerComp)
	{
		FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);
	}
	else
	{
		UE_LOG(LogTemp, Verbose, TEXT("Not found OwnerComp"))
	}
}
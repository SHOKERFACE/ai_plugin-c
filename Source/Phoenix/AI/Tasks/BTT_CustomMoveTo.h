// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "Navigation/PathFollowingComponent.h"
#include "BTT_CustomMoveTo.generated.h"

/**
 * 
 */
UCLASS()
class PHOENIX_API UBTT_CustomMoveTo : public UBTTaskNode
{
	GENERATED_BODY()

public:
	 UBTT_CustomMoveTo();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UFUNCTION()
	void OnMoveCompleted(const FAIRequestID RequestID, const EPathFollowingResult::Type Result);

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float RandomPointInNavigableRadius = 100.0f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FBlackboardKeySelector MoveToLocation;

	UPROPERTY()
	AAIController* MyController;
	
	

	
};

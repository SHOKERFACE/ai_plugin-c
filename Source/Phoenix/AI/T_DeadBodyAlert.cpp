// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/T_DeadBodyAlert.h"
#include "NavAreas/NavArea_Obstacle.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Components/SphereComponent.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"

// Sets default values
AT_DeadBodyAlert::AT_DeadBodyAlert()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Comp"));
	Sphere->SetSphereRadius(32.0f);
	Sphere->Mobility = EComponentMobility::Movable;
	Sphere->AreaClass = FNavigationSystem::GetDefaultObstacleArea();
	Sphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Sphere->SetCollisionResponseToChannel(ECC_Visibility,ECR_Ignore);
	Sphere->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);
	Sphere->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);
	Sphere->SetCollisionResponseToChannel(ECC_WorldStatic,ECR_Overlap);
	Sphere->SetCollisionResponseToChannel(ECC_WorldDynamic,ECR_Overlap);
	Sphere->SetCollisionResponseToChannel(ECC_Pawn,ECR_Overlap);
	Sphere->SetCollisionResponseToChannel(ECC_PhysicsBody,ECR_Overlap);
	Sphere->SetCollisionResponseToChannel(ECC_Vehicle,ECR_Overlap);
	Sphere->SetCollisionResponseToChannel(ECC_Destructible,ECR_Ignore);
	
	StimuliSourceComponent = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("Stimuli Source Component"));
	
}


// Called when the game starts or when spawned
void AT_DeadBodyAlert::BeginPlay()
{
	Super::BeginPlay();
	
}




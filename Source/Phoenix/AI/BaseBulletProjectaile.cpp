

#include "AI/BaseBulletProjectaile.h"

#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ABaseBulletProjectaile::ABaseBulletProjectaile()
{
	PrimaryActorTick.bCanEverTick = false;
	
	CollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	CollisionComponent->InitSphereRadius(5.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Block);
	CollisionComponent->bReturnMaterialOnMove = true;
	SetRootComponent(CollisionComponent);

	ProjectileSphere = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileSphere"));
	ProjectileSphere->SetupAttachment(GetRootComponent());
	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("MovementComponent");
	MovementComponent->InitialSpeed = 10000.0f;
	MovementComponent->MaxSpeed = 10000.0f;
	MovementComponent->ProjectileGravityScale = 0.0f;
	MovementComponent->Velocity = FVector(20000.0f, 0.0f, 0.0f);
	MovementComponent->bRotationFollowsVelocity = true;
	
	InitialLifeSpan = 10.0f;
}

void ABaseBulletProjectaile::BeginPlay()
{
	Super::BeginPlay();
	CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true);
	ProjectileSphere->IgnoreActorWhenMoving(GetOwner(), true);

	CollisionComponent->OnComponentHit.AddDynamic(this, &ABaseBulletProjectaile::OnProjectileHit);
}

void ABaseBulletProjectaile::OnProjectileHit(UPrimitiveComponent* HitComponent,
									 AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
									 const FHitResult& Hit)
{
	if (!GetWorld()) return;
	MovementComponent->StopMovementImmediately();
	Destroy();
}


